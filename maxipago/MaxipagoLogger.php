<?php

namespace maxipago;

use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Logger;

class MaxipagoLogger
{
	private $logger;

	public function __construct(public string $loggerName)
	{
		$this->logger = new Logger($this->loggerName);

		$this->logger->pushHandler(new StreamHandler(__DIR__ . '/../logs/'. $this->loggerName . 'xml.log', Level::Debug));
	}

	public function log($message, $level = Level::Debug):void
	{
		$this->logger->log($level, $message);
	}
}