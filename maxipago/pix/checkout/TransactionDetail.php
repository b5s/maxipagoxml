<?php

namespace maxipago\pix\checkout;

use DOMDocument;
use DOMElement;
use DOMException;
use maxipago\pix\exceptions\InvalidValueException;

class TransactionDetail
{
	/**
	 * @param int                   $pixExpirationTime   Tempo de vida da cobrança em segundos. A partir da geração do
	 *                                                   QR Code.
	 * @param string                $purchaseDescription Descrição da transação PIX. Ex: descrição do que está sendo
	 *                                                   pago
	 * @param PurchaseInformation[] $extraInfo           Detalhamento da compra
	 * @throws InvalidValueException
	 */
	public function __construct(
		private readonly int    $pixExpirationTime,
		private readonly string $purchaseDescription,
		private readonly array  $extraInfo
	)
	{
		foreach ($this->extraInfo as $information) {
			if (!is_a($information, PurchaseInformation::class)) {
				throw new InvalidValueException('The extraInfo array must only have instances of ' . PurchaseInformation::class);
			}
		}
	}

	/**
	 * @param DOMDocument $xml
	 * @return DOMElement
	 * @throws DOMException
	 */
	public function getTransactionDetailElement(DOMDocument $xml): DOMElement
	{
		$transactionDetailElement = $xml->createElement("transactionDetail");
		$payTypeElement = $xml->createElement("payType");
		$pixElement = $xml->createElement("pix");
		$extraInfo = $xml->createElement("extraInfo");

		foreach ($this->extraInfo as $info) {
			$extraInfo->append($info->getInfoElement($xml));
		}

		$pixElement->append(
			$xml->createElement("expirationTime", $this->pixExpirationTime),
			$xml->createElement("paymentInfo", $this->purchaseDescription),
			$extraInfo
		);

		$payTypeElement->appendChild($pixElement);
		$transactionDetailElement->appendChild($payTypeElement);

		return $transactionDetailElement;
	}
}