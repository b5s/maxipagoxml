<?php

namespace maxipago\pix\checkout;

class Payment
{
	/**
	 * @param float $totalPrice Valor do pedido. Os decimais devem ser separados por ponto ("."). Ex: 14.2
	 */
	public function __construct(private readonly float $totalPrice){

	}

	/**
	 * @param \DOMDocument $xml
	 * @return \DOMElement
	 * @throws \DOMException
	 */
	public function getPaymentElement(\DOMDocument $xml): \DOMElement{
		$paymentElement = $xml->createElement("payment");
		$paymentElement->appendChild($xml->createElement("chargeTotal", sprintf("%.2f", $this->totalPrice)));

		return $paymentElement;
	}
}