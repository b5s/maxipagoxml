<?php

namespace maxipago\pix\checkout;

class Document
{
	/**
	 * @param string $documentType O nó não é obrigatório, mas caso seja utilizado, o deverá ser preenchido. Documentos Aceitos: CPF, RG, CNPJ, Passaporte, Cadastro Estadual, Cadastro Municipa, Carteira de Trabalho e Titulo Eleitoral
	 * @param string $documentValue Dados do documento
	 */
	public function __construct(private readonly string $documentType, private readonly string $documentValue){

	}

	/**
	 * @param \DOMDocument $xml
	 * @return \DOMElement
	 * @throws \DOMException
	 */
	public function getDocumentElement(\DOMDocument $xml):\DOMElement{
		$documentElement = $xml->createElement("document");

		$documentElement->append(
			$xml->createElement("documentType", $this->documentType),
			$xml->createElement("documentValue", $this->documentValue)
		);

		return $documentElement;
	}
}