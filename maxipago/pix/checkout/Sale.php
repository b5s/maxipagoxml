<?php

namespace maxipago\pix\checkout;

class Sale
{
	/**
	 * @param string $processorId Código da adquirente que irá processar a transação: Simulador de teste = 1 Rede = 2 GetNet = 3 Cielo = 4
	 * @param string $referenceNum Identificador do pedido no estabelecimento. Este campo aceita apenas valores alfanuméricos e deve ser único.
	 * @param string $fraudCheck Y/N
	 * @param Payment $payment
	 * @param string|null $costumerIdExt CPF do comprador
	 * @param Billing|null $billing
	 * @param TransactionDetail|null $transactionDetail
	 */
	public function __construct(
		private readonly string    $processorId,
		private readonly string    $referenceNum,
		private readonly string    $fraudCheck,
		private readonly Payment   $payment,
		private readonly ?string   $costumerIdExt = null,
		private readonly ?Billing  $billing = null,
		private readonly ?TransactionDetail $transactionDetail = null
	)
	{

	}

	/**
	 * @param \DOMDocument $xml
	 * @return \DOMElement
	 * @throws \DOMException
	 */
	public function getSaleElement(\DOMDocument $xml): \DOMElement
	{
		$saleElement = $xml->createElement("sale");

		$nodes = [
			$xml->createElement("processorID", $this->processorId),
			$xml->createElement('referenceNum', $this->referenceNum),
			$xml->createElement('fraudCheck', $this->fraudCheck),
		];

		if (!is_null($this->costumerIdExt)) {
			$nodes[] = $xml->createElement('customerIdExt', $this->costumerIdExt); //can be null
		}

		if (!is_null($this->billing)) {
			$nodes[] = $this->billing->getBillingElement($xml); //can be null
		}

		if (!is_null($this->transactionDetail)) {
			$nodes[] = $this->transactionDetail->getTransactionDetailElement($xml); // can be null
		}

		$nodes[] = $this->payment->getPaymentElement($xml);

		foreach ($nodes as $node) {
			$saleElement->appendChild($node);
		}

		return $saleElement;
	}
}