<?php

namespace maxipago\pix\checkout;

class PurchaseInformation
{
	/**
	 * @param string $name Info Extra Nome. Ex: produto ou serviço
	 * @param float $value Info Extra Valor. Ex: produto ou serviço //deve ser string?
	 */
	public function __construct(private readonly string $name, private readonly float $value){

	}

	/**
	 * @param \DOMDocument $xml
	 * @return \DOMElement
	 * @throws \DOMException
	 */
	public function getInfoElement(\DOMDocument $xml): \DOMElement{
		$infoElement = $xml->createElement("info");

		$infoElement->append(
			$xml->createElement("name", $this->name),
			$xml->createElement("value", "R$" . sprintf("%.2f", $this->value))
		);

		return $infoElement;
	}
}