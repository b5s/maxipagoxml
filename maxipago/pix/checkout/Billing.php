<?php

namespace maxipago\pix\checkout;

use DOMDocument;
use DOMElement;
use DOMException;
use maxipago\pix\exceptions\InvalidValueException;

class Billing
{
	/**
	 * @param string|null $name
	 * @param string|null $address Logradouro da entrega (sem acento ou caracteres especiais)
	 * @param string|null $address2 Complemento do logradouro de cobrança da transação (sem acentos e caracteres especiais).
	 * @param string|null $district Bairro do logradouro de cobrança (sem acento ou caractere especial)
	 * @param string|null $city Cidade do logradouro de cobrança (sem acento e caracteres especiais)
	 * @param string|null $state Estado do logradouro de cobrança (sem acento e caracteres especiais)
	 * @param string|null $postalcode CEP do logradouro de cobrança (sem acento e caracteres especiais)
	 * @param string|null $country País do comprador
	 * @param string|null $phone
	 * @param string|null $email E-mail do pagador. Obrigatório utilizar "@"
	 * @param Document[] $documents
	 * @throws \maxipago\pix\exceptions\InvalidValueException
	 */
	public function __construct(
		private readonly ?string $name = null,
		private readonly ?string $address = null,
		private readonly ?string $address2 = null,
		private readonly ?string $district = null,
		private readonly ?string $city = null,
		private readonly ?string $state = null,
		private readonly ?string $postalcode = null,
		private readonly ?string $country = null,
		private readonly ?string $phone = null,
		private readonly ?string $email = null,
		private readonly array $documents = []
	)
	{
		foreach ($this->documents as $document) {
			if (!is_a($document, Document::class)) {
				throw new InvalidValueException("A propriedade documents deve ser um array da classe " . Document::class);
			}
		}
	}

	/**
	 * @param DOMDocument $xml
	 * @return DOMElement
	 * @throws DOMException
	 */
	public function getBillingElement(DOMDocument $xml): DOMElement
	{
		$billingElement = $xml->createElement("billing");
		if (count($this->documents)){
			$documentElement = $xml->createElement("documents");
			foreach ($this->documents as $document) {
				$documentElement->appendChild($document->getDocumentElement($xml));
			}
		}

		$billingElementChild = [];

		foreach ($this->getExistingProperties($xml) as $propertyValue){
			$billingElementChild[] = $propertyValue;
		}

		if (isset($documentElement)){
			$billingElementChild[] = $documentElement;
		}

		$billingElement->append(...$billingElementChild);
		return $billingElement;
	}

	/**
	 * @param DOMDocument $xml
	 * @return \Generator
	 * @throws DOMException
	 */
	private function getExistingProperties(DOMDocument $xml): \Generator
	{
		foreach (get_object_vars($this) as $propertyName=>$propertyValue){
			if (!is_array($this->{$propertyName}) && !is_null($this->{$propertyName})){
				yield $xml->createElement($propertyName, $propertyValue);
			}
		}
	}
}