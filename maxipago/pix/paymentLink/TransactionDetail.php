<?php

namespace maxipago\pix\paymentLink;

use DOMDocument;
use DOMElement;
use maxipago\pix\Enums\YesOrNoEnum;

class TransactionDetail
{
	/**
	 * @param string      $description
	 * @param string      $emailSubject
	 * @param string      $expirationDate Formato: MM/DD/YYYY
	 * @param float       $amount
	 * @param YesOrNoEnum $acceptPix
	 */
	public function __construct(
		private readonly string      $description,
		private readonly string      $emailSubject,
		private readonly string      $expirationDate,
		private readonly float       $amount,
		private readonly YesOrNoEnum $acceptPix = YesOrNoEnum::Yes
	)
	{

	}

	public function getTransactionDetailXml(DOMDocument $xml): DOMElement
	{
		$transactionDetailElement = $xml->createElement("transactionDetail");
		$payTypeElement = $xml->createElement("payType");
		$creditCardElement = $xml->createElement("creditCard");

		$creditCardElement->append(
			$xml->createElement("amount", sprintf("%.2f",$this->amount))
		);

		$payTypeElement->append($creditCardElement);

		$transactionDetailElement->append(
			$xml->createElement("description", $this->description),
			$xml->createElement("emailSubject", $this->emailSubject),
			$xml->createElement("expirationDate", $this->expirationDate),
			$xml->createElement("acceptPix", $this->acceptPix->value),
			$payTypeElement
		);

		return $transactionDetailElement;
	}
}