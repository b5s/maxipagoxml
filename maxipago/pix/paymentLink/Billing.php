<?php

namespace maxipago\pix\paymentLink;

use DOMDocument;
use DOMElement;

class Billing
{
	/**
	 * @param string $email
	 * @param string $language 'Ex: pt'
	 * @param string $firstName
	 */
	public function __construct(
		private readonly string $email,
		private readonly string $language,
		private readonly string $firstName
	)
	{

	}

	public function getBillingXml(DOMDocument $xml): DOMElement
	{
		$billingElement = $xml->createElement("billing");

		$billingElement->append(
			$xml->createElement("email", $this->email),
			$xml->createElement("language", $this->language),
			$xml->createElement("firstName", $this->firstName)
		);

		return $billingElement;
	}
}