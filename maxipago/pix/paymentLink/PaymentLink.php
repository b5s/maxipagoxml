<?php

namespace maxipago\pix\paymentLink;

use DOMDocument;
use maxipago\pix\Enums\YesOrNoEnum;

class PaymentLink
{
	/**
	 * @param YesOrNoEnum       $consumerAuthentication
	 * @param string            $referenceNum ID do pedido gerado pelo estabelecimento
	 * @param YesOrNoEnum       $fraudCheck
	 * @param Billing           $billing
	 * @param TransactionDetail $transactionDetail
	 */
	public function __construct(
		private readonly YesOrNoEnum       $consumerAuthentication,
		private readonly string            $referenceNum,
		private readonly YesOrNoEnum       $fraudCheck,
		private readonly Billing           $billing,
		private readonly TransactionDetail $transactionDetail
	)
	{

	}

	public function getPaymentLinkXml(string $storeId, string $merchantKey): string
	{
		$xml = new DOMDocument();
		$xml->preserveWhiteSpace = false;
		$xml->formatOutput = true;

		$apiRequestElement = $xml->createElement("api-request");
		$verificationElement = $xml->createElement('verification');

		$verificationElement->append(
			$xml->createElement('merchantId', $storeId),
			$xml->createElement('merchantKey', $merchantKey)
		);

		$requestElement = $xml->createElement("request");

		$requestElement->append(
			$xml->createElement('consumerAuthentication', $this->consumerAuthentication->value),
			$xml->createElement('referenceNum', $this->referenceNum),
			$xml->createElement('fraudCheck', $this->fraudCheck->value),
			$this->billing->getBillingXml($xml),
			$this->transactionDetail->getTransactionDetailXml($xml)
		);

		$apiRequestElement->append(
			$verificationElement,
			$xml->createElement("command", "add-payment-order"),
			$requestElement
		);

		return $xml->saveXML($apiRequestElement);
	}
}