<?php

namespace maxipago\pix\Enums;

enum YesOrNoEnum: string
{
	case Yes = "Y";
	case No = "N";
}