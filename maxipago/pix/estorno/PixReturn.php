<?php

namespace maxipago\pix\estorno;

use DOMDocument;
use DOMElement;

class PixReturn
{
	/**
	 * @param string $orderId ID do pedido, gerado pela maxiPago!. Deve-se salvar este campo para futuras referências ao pedido.
	 * @param string $referenceNum ID do pedido gerado pelo estabelecimento
	 * @param float $chargeTotal Valor a ser estornado ao consumidor. Os decimais devem ser separados por ponto ("."). Ex: 14.2
	 */
	public function __construct(
		private readonly string $orderId,
		private readonly string $referenceNum,
		private readonly float $chargeTotal
	)
	{

	}

	/**
	 * @param DOMDocument $xml
	 * @return DOMElement
	 * @throws \DOMException
	 */
	public function getPixReturnElement(DOMDocument $xml = new DOMDocument(encoding: "UTF-8")): DOMElement
	{
		$pixReturnElement = $xml->createElement("pixReturn");
		$paymentElement = $xml->createElement("payment");
		$paymentElement->append($xml->createElement("chargeTotal", sprintf("%.2f",$this->chargeTotal)));
		$pixReturnElement->append(
			$xml->createElement("orderID", $this->orderId),
			$xml->createElement("referenceNum", $this->referenceNum),
			$paymentElement
		);

		return $pixReturnElement;
	}
}