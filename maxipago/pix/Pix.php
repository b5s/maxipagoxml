<?php

namespace maxipago\pix;

use DOMDocument;
use DOMElement;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use maxipago\MaxipagoLogger;
use maxipago\pix\checkout\Sale;
use maxipago\pix\dto\GeneratedPix as GeneratedPixDTO;
use maxipago\pix\dto\PaymentLink as PaymentLinkDTO;
use maxipago\pix\estorno\PixReturn;
use maxipago\pix\exceptions\{ExternalRequestException, MaxipagoApiErrorException, MaxipagoExceptionAbstract, XmlException};
use maxipago\pix\paymentLink\PaymentLink;
use SimpleXMLElement;

/**
 * @see https://www.maxipago.com/developers/pix/
 */
class Pix
{
	private static string $productionApiUrl = "https://api.maxipago.net/UniversalAPI/postXML";
	private static string $stagingApiUrl = "https://testapi.maxipago.net/UniversalAPI/postXML";
	private static string $productionApiUrlPaymentLink = "https://api.maxipago.net/UniversalAPI/postAPI";
	private static string $stagingApiUrlPaymentLink = "https://testapi.maxipago.net/UniversalAPI/postAPI";

	/**
	 * @param string $storeId        ID da loja na maxiPago!
	 * @param string $merchantKey    Chave da loja na maxiPago!
	 * @param string $apiVersion     Versão da API
	 * @param bool   $productionMode Indica se a API deve ser executada em modo de produção ou homologação.
	 * @param bool   $debugMode      Utilizado para o desenvolvimento da biblioteca.
	 */
	public function __construct(
		private readonly string $storeId,
		private readonly string $merchantKey,
		private readonly string $apiVersion = "3.1.1.15",
		private bool            $productionMode = false,
		private readonly bool   $debugMode = false
	)
	{
	}

	/**
	 * @param PaymentLink $paymentLink
	 * @return PaymentLinkDTO
	 * @throws ExternalRequestException
	 * @throws GuzzleException
	 * @throws MaxipagoApiErrorException
	 * @throws MaxipagoExceptionAbstract
	 * @throws XmlException
	 */
	public function paymentLink(PaymentLink $paymentLink): PaymentLinkDTO
	{
		try{
			$xml = $paymentLink->getPaymentLinkXml($this->storeId, $this->merchantKey);

			if ($this->debugMode) {
				(new MaxipagoLogger("PaymentLink"))->log($xml);
				return new PaymentLinkDTO(new SimpleXMLElement($xml));
			}
		}catch (MaxipagoApiErrorException $e){
			throw $e;
		}catch (\Throwable $t){
			throw new XmlException("Error generating XML for PaymentLink", previous: $t);
		}

		return $this->makeRequest(xml: $xml, fetchIntoClass: PaymentLinkDTO::class, isPaymentLink: true);
	}

	/**
	 * @param bool $productionMode
	 * @return $this
	 */
	public function productionMode(bool $productionMode): self
	{
		$this->productionMode = $productionMode;
		return $this;
	}

	/**
	 * @param Sale $sale
	 * @return GeneratedPixDTO
	 * @throws ExternalRequestException
	 * @throws GuzzleException
	 * @throws MaxipagoApiErrorException
	 * @throws MaxipagoExceptionAbstract
	 * @throws XmlException
	 */
	public function checkout(Sale $sale): GeneratedPixDTO
	{
		try{
			$xml = $this->getCheckoutXml($sale);
			if ($this->debugMode) {
				(new MaxipagoLogger("CheckOut"))->log($xml);
				return new GeneratedPixDTO(new SimpleXMLElement($xml));
			}
		}catch (MaxipagoApiErrorException $e){
			throw $e;
		}catch (\Throwable $t){
			throw new XmlException("Error generating XML for Checkout", previous: $t);
		}

		return $this->makeRequest($xml);
	}

	/**
	 * @param Sale $sale
	 * @return string
	 * @throws XmlException
	 */
	public function getCheckoutXml(Sale $sale): string
	{
		try{
			$xml = new DOMDocument(encoding: "UTF-8");
			return $this->createXML($xml, $sale->getSaleElement($xml));
		}catch (\Throwable $t){
			throw new XmlException('Error generating checkout XML', previous: $t);
		}
	}

	/**
	 * @param DOMDocument $xml
	 * @param DOMElement  $element
	 * @return string
	 * @throws XmlException
	 */
	private function createXML(DOMDocument $xml, DOMElement $element): string
	{
		try{
			$xml->preserveWhiteSpace = false;
			$xml->formatOutput = true;

			//transaction-request element
			$transactionElement = $xml->createElement("transaction-request");

			//verification tag
			$verificationElement = $xml->createElement('verification');

			$verificationElement->append(
				$xml->createElement('merchantId', $this->storeId),
				$xml->createElement('merchantKey', $this->merchantKey)
			);

			$orderElement = $xml->createElement('order');
			$orderElement->appendChild($element);

			$transactionElement->append(
				$xml->createElement("version", $this->apiVersion),
				$verificationElement,
				$orderElement
			);

			$xml->appendChild($transactionElement);

			return $xml->saveXML();
		}catch (\Throwable $t){
			throw new XmlException('Error generating XML', previous: $t);
		}

	}


	/**
	 * @param string $transactionId ID da transação, gerado pela maxiPago!. Deve-se salvar este campo para futuras
	 *                              referências ao pedido.
	 * @return GeneratedPixDTO
	 * @throws ExternalRequestException
	 * @throws GuzzleException
	 * @throws MaxipagoApiErrorException
	 * @throws MaxipagoExceptionAbstract
	 * @throws XmlException
	 */
	public function voidPix(string $transactionId): GeneratedPixDTO
	{
		try{
			$xml = $this->getVoidPixXml($transactionId);

			if ($this->debugMode) {
				(new MaxipagoLogger("VoidPix"))->log($xml);
				return new GeneratedPixDTO(new SimpleXMLElement($xml));
			}
		}catch (MaxipagoApiErrorException $e){
			throw $e;
		}catch (\Throwable $t){
			throw new XmlException("Error generating XML for VoidPix", previous: $t);
		}

		return $this->makeRequest($xml);
	}

	/**
	 * @param string $transactionId
	 * @return string
	 * @throws XmlException
	 */
	private function getVoidPixXml(string $transactionId): string
	{
		try{
			$xml = new DOMDocument(encoding: "UTF-8");
			$voidElement = $xml->createElement('void');
			$voidElement->appendChild($xml->createElement("transactionID", $transactionId));

			return $this->createXML($xml, $voidElement);
		}catch (\Throwable $t){
			throw new XmlException('Error generating Void Pix XML', previous: $t);
		}
	}

	/**
	 * @param PixReturn $estorno
	 * @return GeneratedPixDTO
	 * @throws ExternalRequestException
	 * @throws GuzzleException
	 * @throws MaxipagoApiErrorException
	 * @throws MaxipagoExceptionAbstract
	 * @throws XmlException
	 */
	public function chargeback(PixReturn $estorno): GeneratedPixDTO
	{
		try{
			$xml = $this->getChargebackXml($estorno);

			if ($this->debugMode) {
				(new MaxipagoLogger("Chargeback"))->log($xml);
				return new GeneratedPixDTO(new SimpleXMLElement($xml));
			}
		}catch (MaxipagoApiErrorException $e){
			throw $e;
		}catch (\Throwable $t){
			throw new XmlException("Error generating XML for Chargeback", previous: $t);
		}


		return $this->makeRequest($xml);
	}

	/**
	 * @param PixReturn $estorno
	 * @return string
	 * @throws XmlException
	 */
	private function getChargebackXml(PixReturn $estorno): string
	{
		try{
			$xml = new DOMDocument(encoding: "UTF-8");
			return $this->createXML($xml, $estorno->getPixReturnElement($xml));
		}catch (\Throwable $t){
			throw new XmlException('Error generating Chargeback XML', previous: $t);
		}
	}

	/**
	 * @param string $xml
	 * @param string $fetchIntoClass
	 * @param bool   $isPaymentLink
	 * @return GeneratedPixDTO|PaymentLinkDTO
	 * @throws ExternalRequestException
	 * @throws MaxipagoExceptionAbstract
	 * @throws XmlException
	 * @throws GuzzleException
	 */
	private function makeRequest(string $xml, string $fetchIntoClass = GeneratedPixDTO::class, bool $isPaymentLink = false): GeneratedPixDTO|PaymentLinkDTO
	{
		$client = new Client(['verify' => false]);

		if ($isPaymentLink) {
			$uri = $this->productionMode ? self::$productionApiUrlPaymentLink : self::$stagingApiUrlPaymentLink;
		} else {
			$uri = $this->productionMode ? self::$productionApiUrl : self::$stagingApiUrl;
		}

		$response = $client->request(
			"POST",
			$uri,
			[
				'headers' => [
					'Content-Type' => 'application/xml',
				],
				'body'    => $xml,
				'ssl'     => [
					'allow_self_signed' => true,
				],
			]
		);

		if ($response->getStatusCode() !== 200) {
			throw new ExternalRequestException("Status: " . $response->getStatusCode());
		}

		$responseBody = $response->getBody();

		try{
			return new $fetchIntoClass(new SimpleXMLElement($responseBody->read($responseBody->getSize())));
		}catch (MaxipagoExceptionAbstract $e) {
			throw $e;
		}catch (\Throwable $t){
			throw new XmlException("Error parsing XML response", previous: $t);
		}
	}

}