<?php

namespace maxipago\pix\exceptions;

use maxipago\pix\dto\GeneratedPix;
use maxipago\pix\dto\PaymentLink;
use Throwable;

class MaxipagoApiErrorException extends MaxipagoExceptionAbstract
{
	public function __construct(string $message = "", int|string $code = 0, ?Throwable $previous = null, private readonly null|GeneratedPix|PaymentLink $returnedData = null)
	{
		parent::__construct($message, (int)$code, $previous);

	}

	public function getReturnedData(): ?GeneratedPix
	{
		return $this->returnedData;
	}
}