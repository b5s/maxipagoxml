<?php

namespace maxipago\pix\dto;

use maxipago\pix\exceptions\MaxipagoApiErrorException;
use SimpleXMLElement;

class PaymentLink
{
	private string $errorCode;
	private string $errorMessage;
	private string $command;
	private string $time;
	private PaymentLinkResult $result;

	/**
	 * @param SimpleXMLElement $xml
	 * @throws MaxipagoApiErrorException
	 */
	public function __construct(SimpleXMLElement $xml)
	{
		$this->errorCode = $xml->errorCode;
		$this->errorMessage = $xml->errorMessage;
		$this->command = $xml->command;
		$this->time = $xml->time;
		$this->result = new PaymentLinkResult(
			$xml->result->pay_order_id,
			$xml->result->message,
			$xml->result->url,
			$xml->result->pixOnlineDebitUrl,
			$xml->result->pixQrCode
		);

		if ($this->errorMessage){
			throw new MaxipagoApiErrorException(message: $this->errorMessage, code: $this->errorCode, returnedData: $this);
		}
	}

	/**
	 * @return string
	 */
	public function getErrorCode(): string
	{
		return $this->errorCode;
	}

	/**
	 * @return string
	 */
	public function getErrorMessage(): string
	{
		return $this->errorMessage;
	}

	/**
	 * @return string
	 */
	public function getCommand(): string
	{
		return $this->command;
	}

	/**
	 * @return string
	 */
	public function getTime(): string
	{
		return $this->time;
	}

	/**
	 * @return PaymentLinkResult
	 */
	public function getResult(): PaymentLinkResult
	{
		return $this->result;
	}
}