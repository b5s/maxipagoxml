<?php

namespace maxipago\pix\dto;

class PaymentLinkResult
{
	public function __construct(
		private readonly ?string $payOrderId,
		private readonly ?string $message,
		private readonly ?string $url,
		private readonly ?string $pixOnlineDebitUrl,
		private readonly ?string $pixQrCode
	)
	{

	}

	/**
	 * @return null|string
	 */
	public function getPixQrCode(): ?string
	{
		return $this->pixQrCode;
	}

	/**
	 * @return null|string
	 */
	public function getPayOrderId(): ?string
	{
		return $this->payOrderId;
	}

	/**
	 * @return null|string
	 */
	public function getMessage(): ?string
	{
		return $this->message;
	}

	/**
	 * @return null|string
	 */
	public function getUrl(): ?string
	{
		return $this->url;
	}

	/**
	 * @return null|string
	 */
	public function getPixOnlineDebitUrl(): ?string
	{
		return $this->pixOnlineDebitUrl;
	}
}