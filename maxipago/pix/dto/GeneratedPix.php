<?php

namespace maxipago\pix\dto;

use maxipago\pix\exceptions\MaxipagoApiErrorException;
use SimpleXMLElement;

class GeneratedPix
{
	private string $orderId;
	private string $referenceNum;
	private string $transactionId;
	private string $transactionTimestamp;
	private string $responseCode;
	private string $processorCode;
	private string $processorMessage;
	private string $processorName;
	private string $processorTransactionId;
	private string $processorReferenceNumber;
	private string $onlineDebitUrl;
	private string $emv;
	private string $base64Image;
	private string $errorMessage;
	private string $authCode;
	private string $responseMessage;
	private string $avsResponseCode;
	private string $cvvResponseCode;
	private string $errorCode;
	private string $errorMsg;

	/**
	 * @param SimpleXMLElement $xml
	 * @throws MaxipagoApiErrorException
	 */
	public function __construct(SimpleXMLElement $xml)
	{
		$this->orderId = $xml->orderID;
		$this->referenceNum = $xml->referenceNum;
		$this->transactionId = $xml->transactionID;
		$this->transactionTimestamp = $xml->transactionTimestamp;
		$this->responseCode = $xml->responseCode;
		$this->processorCode = $xml->processorCode;
		$this->processorMessage = $xml->processorMessage;
		$this->processorName = $xml->processorName;
		$this->processorTransactionId = $xml->processorTransactionID;
		$this->processorReferenceNumber = $xml->processorReferenceNumber;
		$this->onlineDebitUrl = $xml->onlineDebitUrl;
		$this->emv = $xml->emv;
		$this->base64Image = $xml->imagem_base64;
		$this->errorMessage = $xml->errorMessage;
		$this->authCode = $xml->authCode;
		$this->responseMessage = $xml->responseMessage;
		$this->avsResponseCode = $xml->avsResponseCode;
		$this->cvvResponseCode = $xml->cvvResponseCode;
		$this->errorCode = $xml->errorCode;
		$this->errorMsg = $xml->errorMsg;

		if ((string)$this->errorMessage || (string)$this->errorMsg){
			throw new MaxipagoApiErrorException(message: $this->errorMessage ?: $this->errorMsg, code: $this->errorCode, returnedData: $this);
		}
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getErrorCode(): SimpleXMLElement|string
	{
		return $this->errorCode;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getErrorMsg(): SimpleXMLElement|string
	{
		return $this->errorMsg;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getErrorMessage(): SimpleXMLElement|string
	{
		return $this->errorMessage;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getAuthCode(): SimpleXMLElement|string
	{
		return $this->authCode;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getResponseMessage(): SimpleXMLElement|string
	{
		return $this->responseMessage;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getAvsResponseCode(): SimpleXMLElement|string
	{
		return $this->avsResponseCode;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getCvvResponseCode(): SimpleXMLElement|string
	{
		return $this->cvvResponseCode;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getOrderId(): SimpleXMLElement|string
	{
		return $this->orderId;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getReferenceNum(): SimpleXMLElement|string
	{
		return $this->referenceNum;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getTransactionId(): SimpleXMLElement|string
	{
		return $this->transactionId;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getTransactionTimestamp(): SimpleXMLElement|string
	{
		return $this->transactionTimestamp;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getResponseCode(): SimpleXMLElement|string
	{
		return $this->responseCode;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getProcessorCode(): SimpleXMLElement|string
	{
		return $this->processorCode;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getProcessorMessage(): SimpleXMLElement|string
	{
		return $this->processorMessage;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getProcessorName(): SimpleXMLElement|string
	{
		return $this->processorName;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getProcessorTransactionId(): SimpleXMLElement|string
	{
		return $this->processorTransactionId;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getProcessorReferenceNumber(): SimpleXMLElement|string
	{
		return $this->processorReferenceNumber;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getOnlineDebitUrl(): SimpleXMLElement|string
	{
		return $this->onlineDebitUrl;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getEmv(): SimpleXMLElement|string
	{
		return $this->emv;
	}

	/**
	 * @return SimpleXMLElement|string
	 */
	public function getBase64Image(): SimpleXMLElement|string
	{
		return $this->base64Image;
	}
}