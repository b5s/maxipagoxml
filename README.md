**PIX Maxipago**

Para mais informações sobre o funcionamento da biblioteca, seus campos e valores, consultar [https://www.maxipago.com/developers/pix/](https://www.maxipago.com/developers/pix/)

Instanciar a classe maxipago\pix\Pix e indicar no construtor o _storeId_ e a _merchantKey_, assim como o modo de execução. A versão da API deve ser modificada apenas
em caso de necessidade, assim como o debugMode.

Por padrão, a biblioteca utiliza a API na versão 3.1.1.15.

```php
new maxipago\pix\Pix(storeId: 'xxxx', merchantKey: 'yyyy');
```

Por padrão, a biblioteca opera no ambiente de **homologação** para evitar execuções acidentais durante o desenvolvimento, para alterar
para o ambiente de produção, basta indicar no construtor da classe maxipago\pix\Pix ou chamar o método productionMode e marcar o mesmo como true:

```php
new maxipago\pix\Pix(storeId: 'xxxx', merchantKey: 'yyyy', productionMode: true);
```
Ou

```php
$pix = new maxipago\pix\Pix(storeId: 'xxxx', merchantKey: 'yyyy');

$pix->productionMode(true);
```

**A biblioteca não realizara chamadas para nenhuma das API's da Maxipago quando estiver sendo executada em modo de debug.**

**Link de Pagamento:**
Para realizar uma requisição de geração de pix por link de pagamento, deve se utilizar o método paymentLink, que recebe como parâmetro uma instância da classe maxipago\pix\paymentLink\PaymentLink.

```php
$paymentLink = new PaymentLink(
    consumerAuthentication: maxipago\pix\Enums\YesOrNoEnum::No,
    referenceNum: '2009171040',
    fraudCheck: maxipago\pix\Enums\YesOrNoEnum::No,
    billing: new \maxipago\pix\paymentLink\Billing(
        email: 'dev@maxipago.com',
        language: 'pt',
        firstName: 'Dev'
    ),
    transactionDetail: new \maxipago\pix\paymentLink\TransactionDetail(
        description: 'pagamento Smart TV',
        emailSubject: 'Favor efetuar o pagamento',
        expirationDate: '06/17/2022',
        amount: 2.00,
        acceptPix: maxipago\pix\Enums\YesOrNoEnum::Yes
    )
);

$pix = new maxipago\pix\Pix(storeId: 'xxxx', merchantKey: 'yyyy');
$pix->paymentLink($paymentLink);
```

Uma instância de maxipago\pix\dto\PaymentLink será retornada.

**Pix Checkout:**
Para realizar um pagamento Pix no modelo Checkout, basta utilizar o método checkout, que recebe como parâmetro uma instância da classe maxipago\pix\checkout\Sale.

```php
$sale = new Sale(
	processorId: '200',
	referenceNum: 'MXP_PIX_130120211126',
	fraudCheck: 'N', 
	payment: new maxipago\pix\checkout\Payment(totalPrice: 10005.00),
	costumerIdExt: '37568256634',
	billing: new maxipago\pix\checkout\Billing(
		name: 'maxipixPago',
		address: 'Avenida Paulista 123',
		address2: '1 Andar',
		district: 'Paraiso',
		city: 'Sao Paulo',
		state: 'SP',
		postalcode: '01311000',
		country: 'BR',
		phone: '36925873229',
		email: 'testepix@testepix.com',
		documents: [
			new maxipago\pix\checkout\Document(documentType: 'CPF', documentValue: '99907514047'),
		]
	),
	transactionDetail: new maxipago\pix\checkout\TransactionDetail(
		pixExpirationTime: 86400,
		purchaseDescription: 'Smart TV LG 65´ 8K IPS NanoCell, Conexão WiFi e Bluetooth',
		extraInfo: [
			new maxipago\pix\checkout\PurchaseInformation(name: 'TV8k', value: 10000.00),
			new maxipago\pix\checkout\PurchaseInformation(name: 'Garantia Estendida', value: 5.00),
		]
	)
);

$pix->checkout($sale);
```

Uma instância de maxipago\pix\dto\GeneratedPix será retornada.

**Estorno:**
Para realizar uma operação de estorno, basta utilizar o método chargeback, que recebe como parâmetro uma instância da classe maxipago\pix\estorno\PixReturn.

```php
$sale = new Sale(
	processorId: '200',
	referenceNum: 'MXP_PIX_130120211126',
	fraudCheck: 'N',
	payment: new Payment(totalPrice: 10005.00),
	costumerIdExt: '37568256634',
	billing: new Billing(
		name: 'maxipixPago',
		address: 'Avenida Paulista 123',
		address2: '1 Andar',
		district: 'Paraiso',
		city: 'Sao Paulo',
		state: 'SP',
		postalcode: '01311000',
		country: 'BR',
		phone: '36925873229',
		email: 'testepix@testepix.com',
		documents: [
			new Document(documentType: 'CPF', documentValue: '99907514047'),
		]
	),
	transactionDetail: new TransactionDetail(
		pixExpirationTime: 86400,
		purchaseDescription: 'Smart TV LG 65´ 8K IPS NanoCell, Conexão WiFi e Bluetooth',
		extraInfo: [
			new PurchaseInformation(name: 'TV8k', value: 10000.00),
			new PurchaseInformation(name: 'Garantia Estendida', value: 5.00),
		]
	)
);

$result = $pix->checkout($sale);

$chargeback = new PixReturn(
    orderId: $result->getOrderId(), 
    referenceNum: $result->getReferenceNum(), 
    chargeTotal: 10005.00
);
$pix->chargeback($chargeback);
```

Uma instância de maxipago\pix\dto\GeneratedPix será retornada.

**Cancelamento de Pix (VOID):**

Para realizar uma operação de cancelamento de Pix, basta utilizar o método voidPix, passando como parâmetro a transactionId do pix a ser cancelado.

```php
$paymentLink = new PaymentLink(
    consumerAuthentication: maxipago\pix\Enums\YesOrNoEnum::No,
    referenceNum: '2009171040',
    fraudCheck: maxipago\pix\Enums\YesOrNoEnum::No,
    billing: new \maxipago\pix\paymentLink\Billing(
        email: 'dev@maxipago.com',
        language: 'pt',
        firstName: 'Dev'
    ),
    transactionDetail: new \maxipago\pix\paymentLink\TransactionDetail(
        description: 'pagamento Smart TV',
        emailSubject: 'Favor efetuar o pagamento',
        expirationDate: '06/17/2022',
        amount: 2.00,
        acceptPix: 'S'
    )
);

$pix = new maxipago\pix\Pix(storeId: 'xxxx', merchantKey: 'yyyy');
$result = $pix->paymentLink($paymentLink);

$pix->voidPix(transactionId: $result->getTransactionId());
```

Uma instância de maxipago\pix\dto\GeneratedPix será retornada.